# Marcin Garnowski

import numpy
import matplotlib.pyplot as plt


# liczba krawedzi dla grafu o n wierzcholkach
def losowa_liczba_krawedzi(n):
    return numpy.random.randint(n * 2, n * (n - 1))


# graf - macierz krawędzi i wag
def czy_spojny(graf, liczba_kraw):
    licznik_krawedzi = 0
    for lista in graf:
        warunek = False
        for wierzcholek in lista:
            if wierzcholek != 0:
                licznik_krawedzi += 1
                warunek = True
        if warunek == False:
            return False
    if licznik_krawedzi < liczba_kraw:
        return False
    else:
        return True


# n - liczba wierzcholkow
# max - maksymanlna waga
def losowy_graf(n, max):
    liczba_kraw = losowa_liczba_krawedzi(n)
    graf = [[0 for i in range(n)] for j in range(n)]
    while not czy_spojny(graf, liczba_kraw):
        for i in range(n):
            numer = numpy.random.randint(0, n)
            wartosc = numpy.random.randint(0, max)
            graf[i][numer], graf[numer][i] = wartosc, wartosc
            graf[i][i] = 0
    return graf

#dla każdej liczby n z listy N tworzy losowy graf o n krawędziach i maksymalnej wadze max i zapisuje do pliku
def wygeneruj_grafy(max):
    N = [5, 8, 12, 16, 20]
    for i in range(5):
        nazwa_pliku = 'graf' + str(i + 1) + '.txt'
        plik = open(nazwa_pliku, 'w')
        graf = losowy_graf(N[i], max)
        for lista in graf:
            linia = ''
            for element in lista:
                linia += str(element) + (4 - len(str(element))) * ' '
            linia += '\n'
            plik.write(linia)
        plik.close()

#rozdziela graf na listę krawędzi typu [w(i,j), i, j], gdzie i, j to wierzchołki, a w(i,j) to waga krawędzi między nimi
def rozdziel_graf(graf):
    wagi = []
    n = len(graf)
    for i in range(n):
        for j in range(i, n):
            if graf[i][j] != 0:
                wagi.append([graf[i][j], i, j])
    return wagi

#sortuje listę krawędzi niemalejąco pod względem wag
def sortuj_wagi(wagi):
    warunek = True
    n = len(wagi)
    while warunek == True:
        warunek = False
        for i in range(n - 1):
            if wagi[i][0] > wagi[i + 1][0]:
                warunek = True
                a = wagi[i]
                wagi[i] = wagi[i + 1]
                wagi[i + 1] = a

#bąbelkowe sortowanie listy
def sortuj_liste(lista):
    warunek = True
    n = len(lista)
    while warunek == True:
        warunek = False
        for i in range(n - 1):
            if lista[i] > lista[i + 1]:
                warunek = True
                a = lista[i]
                lista[i] = lista[i + 1]
                lista[i + 1] = a

#łączy zbiory A i B, a następnie ustawia ten nowy zbiór jako zbiór dla każdego elementu należącego do A lub B
def polacz_zbiory(A, B, zbiory):
    for x in B:
        if x not in A:
            A.append(x)
    sortuj_liste(A)  # sortowanie, żeby móc później łatwo porównywać listy CHYBA NIEPOTRZEBNE!
    for x in A:
        zbiory[x] = A

#znalezienie krawędzi należących do MST grafu,
def kruskal(graf):
    wagi = rozdziel_graf(graf)
    sortuj_wagi(wagi)
    zbiory = [[i] for i in range(len(graf))]  # zbiory[i] - zbiór, do którego należy element i
    A = []
    for krawedz in wagi:
        if zbiory[krawedz[1]] != zbiory[krawedz[2]]:
            A.append([krawedz[1], krawedz[2]])
            polacz_zbiory(zbiory[krawedz[1]], zbiory[krawedz[2]], zbiory)
    print('MST:',A)
    return A

#tworzy słownik współrzędnych n wierzchołków
def punkty_wykresu(n):
    punkty = {}
    for i in range(n):
        x = numpy.cos(numpy.pi / n * 2 * i)
        y = numpy.sin(numpy.pi / n * 2 * i)
        punkty[i] = [x, y]
    return punkty

#wyświetla graficznie graf i wyróżnia mst
def wyswietl_graf(graf):
    kolor = 'orange'
    n = len(graf)
    punkty = punkty_wykresu(n)
    krawedzie = rozdziel_graf(graf)
    krawedzie = [[i[1], i[2]] for i in krawedzie]
    drzewo = kruskal(graf)
    for krawedz in krawedzie:
        waga = graf[krawedz[0]][krawedz[1]]
        x1, y1 = punkty[krawedz[0]]
        x2, y2 = punkty[krawedz[1]]
        plt.plot([x1,x2], [y1, y2], color = kolor, linestyle = 'dashed', linewidth = 1)
        przesuniecie = numpy.random.randint(0,10)
        plt.text((x1+x2)/2 + .01 * (5 - przesuniecie), (y1+y2)/2 + .01 * (5 - przesuniecie), waga, fontsize=10)

    for krawedz in drzewo:
        x1, y1 = punkty[krawedz[0]]
        x2, y2 = punkty[krawedz[1]]
        plt.plot([x1, x2], [y1, y2], color = kolor, linewidth=4)

    punkty = punkty_wykresu(n)
    punktyX, punktyY = [], []
    punkty_tekst = []
    for i in range(n):
        x, y = punkty[i]
        punktyX.append(x)
        punktyY.append(y)
        punkty_tekst.append(str(i + 1))
    plt.scatter(punktyX, punktyY, c = kolor, s=1000)
    for i in range(n):
        plt.text(punktyX[i] - .05, punktyY[i] - .055, punkty_tekst[i], fontsize=20)
    plt.show()


# C L I
graf = None
polecenie = ''
while polecenie != 'zakoncz':
    polecenie = input('Podaj polecenie (help): ')
    polecenie = polecenie.split()
    if polecenie[0] == 'help':
        print('''Lista komend:
        - generuj - stworzenie nowych losowych grafów,

        - losowy n - stworzenie losowego grafu o n wierzchołkach,

        - nowy n - tworzy nowy graf o n krawędziach,

        - dodaj i, j, w - tworzy krawędź między i-tym a j-tym wierzchołkiem o wadze w,

        - mst - minimalne drzewo rozpinające podanego grafu,

        - spojny - sprawdzenie, czy podany graf jest spójny,

        - zbadaj - tworzy mst dla 5 wygenerowanych grafów.''')
    elif polecenie[0] == 'nowy':
        n = int(polecenie[1])
        graf = [[0 for i in range(n)] for j in range(n)]

    elif polecenie[0] == 'generuj':
        wygeneruj_grafy(9)
        print('Wygenerowano nowe grafy!\n')

    elif polecenie[0] == 'mst':
        n = len(graf)
        if czy_spojny(graf, n-1):
            wyswietl_graf(graf)
        else:
            print('Graf nie jest spójny!\n')

    elif polecenie[0] == 'losowy':
        n = int(polecenie[1])
        graf = losowy_graf(n, 9)
        print('Utworzono losowy graf\n')

    elif polecenie[0] == 'dodaj':
        n = len(graf)
        i, j, w = int(polecenie[1]), int(polecenie[2]), int(polecenie[3])
        i -= 1
        j -= 1
        if i >= n or j >= n:
            print('Niepoprawne wartości!\n')
        else:
            graf[i][j] = w
            graf[j][i] = w
            print('Dodano\n')

    elif polecenie[0] == 'nowy':
        n = int(polecenie[1])
        graf = [[0 for i in range(n)] for j in range(n)]

    elif polecenie[0] == 'zbadaj':
        for i in range(1,6):
            nazwa_pliku = 'graf' + str(i) + '.txt'
            plik = open(nazwa_pliku, 'r')
            graf = plik.readlines()
            plik.close()
            graf = [i.strip() for i in graf]
            graf = [[int(j) for j in i.split()] for i in graf]
            wyswietl_graf(graf)
