# Marcin Garnowski

import numpy as np
import matplotlib.pyplot as plt

# współczynniki - wzięte z zadania 1 z podręcznika
# n - podział pręta (liczba punktów nie licząc końców),
# k - krok czasowy
# M - liczba kroków
# ab - stała temperatura na końcach
n, k, M = 20, 0.001, 300
ab = 0
h = 1 / (n + 1)
s = k / (h ** 2)
w = [np.sin(np.pi * i * h) for i in range(n + 2)]
v = [0 for i in range(n + 2)]

# rod - zbiór M wektorów temperatury pręta (dla każdego kroku)
rod = []

# maksymalna temperatura podczas całego doświadczenia
maks = max(max(w), ab)

# zmiany temperatury
for j in range(1, M + 1):
    v[0] = ab
    v[n + 1] = ab
    t = j * k
    for i in range(1, n + 1):
        v[i] = s * w[i - 1] + (1 - 2 * s) * w[i] + s * w[i + 1]
    print(j, t, v, sep='\n')
    rod.append(v.copy())
    w = v.copy()

# wykres temperatury pręta od czasu
plt.imshow(rod, extent=[0, 1, 0, t], cmap='jet',
           vmin=0, vmax=maks, origin='lowest', aspect='auto')
plt.colorbar()
plt.xlabel("x", fontsize=15)
plt.ylabel("t", rotation=0, fontsize=15)
plt.show()