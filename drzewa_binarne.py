# Marcin Garnowski

import numpy as np

#wyczyszczenie ekranu przez wypisanie 40 pustych linii
def wyczysc_ekran():
    print('\n' * 40)

#lista n losowych elementów z przedziału (0, maks)
def losowa_lista(n, maks):
    A = []
    for i in range(n):
        A.append(np.random.randint(0,maks))
    return A

#ustawienie elementów listy w losowej kolejności, a - naturalna liczba, im większa, tym bardziej losowa kolejność
def tasuj(list, a = 10):
    list_len = len(list)
    for i in range(list_len * a):
        a1 = np.random.randint(list_len)
        a2 = np.random.randint(list_len)
        c = list[a1]
        list[a1] = list[a2]
        list[a2] = c

#klasa węzeł, key - klucz węzła, is_root - sprawdzanie, czy jest korzeniem, is_leaf - czy jest liściem
class wezel:
    def __init__(self, key, parent = None):
        self.key = key
        self.parent = parent
        self.left = None
        self.right = None

    def is_root(self):
        if self.parent is None:
            return True
        else:
            return False

    def is_leaf(self):
        if self.left is None and self.right is None:
            return True
        else:
            return False

#utworzenie korzenia nowego drzewa o kluczu k
def nowy_korzen(k):
    return wezel(k)

#wyszukiwanie elementu o kluczu k w drzewie o korzeniu x, jeśli czy_zwracac_licznik jest True, to funkcja zwróci liczbę operacji
def szukaj(x, k, czy_zwracac_licznik = False):
    licznik = 0
    while x is not None and x.key != k:
        licznik += 1
        if k < x.key:
            x = x.left
        else:
            x = x.right
    if czy_zwracac_licznik:
        return licznik

    elif x.key == k:
        print('Znaleziono', k, '\nLiczba operacji:', licznik)
        return x
    else:
        print('Nie znaleziono', k)
        return None

#dodanie elementu o kluczu k do drzewa o korzeniu x, jeśli czy_zwracac_licznik jest True, to funkcja zwróci liczbę operacji
def dodaj(x, k, czy_zwracac_licznik = False):
    licznik = 0
    rodzic = None

    while True:
        licznik += 1
        if k < x.key:   # lewa gałąź
            if x.left is None:
                x.left = wezel(k,x)
                break

            else:
                rodzic = x
                x = x.left

        else:           # prawa
            if x.right is None:
                x.right = wezel(k,x)
                break

            else:
                x = x.right
    if czy_zwracac_licznik:
        return licznik

    print('Dodano', k, '\nLiczba operacji:', licznik)

#usuwanie elementu o kluczu k z drzewa o korzeniu x, jeśli czy_zwracac_licznik jest True, to funkcja zwróci liczbę operacji
def usun(x, k, czy_zwracac_licznik = False):
    usuwany_wezel = szukaj(x, k)
    if usuwany_wezel is None:
        print('Brak elementu w drzewie')
        return 0

    elif usuwany_wezel.is_root() == True:         # usuwanie korzenia - zabronione
        print('Nie można usunąć korzenia!')

    else:
        licznik = szukaj(x, k, True)
        if usuwany_wezel.is_leaf() == True:       # usuwanie liscia
            licznik += 1
            if usuwany_wezel.key < usuwany_wezel.parent.key:
                usuwany_wezel.parent.left = None

            else:
                usuwany_wezel.parent.right = None

        else:
            lewa = usuwany_wezel.left
            prawa = usuwany_wezel.right
            rodzic = usuwany_wezel.parent

            if prawa is None:
                licznik += 1
                lewa.parent = rodzic
                if k < rodzic.key:
                    rodzic.left = lewa
                else:
                    rodzic.right = lewa

            elif lewa is None:
                licznik += 1
                prawa.parent = rodzic
                if k < rodzic.key:
                    rodzic.left = prawa
                else:
                    rodzic.right = prawa

            else:
                lewy_koniec_prawej = prawa                  # szukanie lewego konca w prawej galezi usuwanego wezla
                while lewy_koniec_prawej.left is not None:
                    licznik += 1
                    lewy_koniec_prawej = lewy_koniec_prawej.left

                lewy_koniec_prawej.left = lewa  # dołączanie lewej gałęzi usuniętego węzła do końca lewej gałęzi nowego węzła
                lewa.parent = lewy_koniec_prawej

                if k < rodzic.key:              # na koniec włożyć .right w miejsce usunietego wezla
                    rodzic.left = prawa
                    prawa.parent = rodzic       # włożenie prawego węzła w miejsce usuniętego

                else:
                    rodzic.right = prawa
                    prawa.parent = rodzic
    print(f'Usunięto {k}\nLiczba operacji: {licznik}')
    if czy_zwracac_licznik:
        return licznik

#zwrócenie klucza węzła x lub oznaczenia braku węzła
def wartosc(x, znak_liscia):
    if x is not None:
        return x.key
    else:
        return znak_liscia


#uzupelnianie spacjami, aby wartość klucza była odpowiednia do wyświetlania
def popraw_klucz(klucz, liczba_cyfr):
    klucz = str(klucz)

    while len(klucz) < liczba_cyfr:
        klucz = ' ' + klucz

    return klucz

#wpisywanie kluczy w macierz, tablica - macierz, klucze - lista kluczy w drzewie, indeks i glebokosc - współrzędne w macierzy
def wpisz_klucze(tablica, klucze, indeks, liczba_cyfr = 4, poprzednia_roznica = 0, glebokosc = 0):   # skorzystac z poprzedniego indeksu?
    if glebokosc < len(tablica) - 1:
        tablica[glebokosc][indeks] = popraw_klucz(klucze[glebokosc].pop(0), liczba_cyfr)  # wpisz klucz przy schodzeniu

        if poprzednia_roznica == 0:
            roznica = indeks - indeks//2
        else:
            roznica = poprzednia_roznica//2
        wpisz_klucze(tablica, klucze, indeks - roznica, liczba_cyfr, roznica, glebokosc + 1)  # lewa  galaz
        wpisz_klucze(tablica, klucze, indeks + roznica, liczba_cyfr, roznica, glebokosc + 1)  # prawa galaz

    else:
        tablica[glebokosc][indeks] = popraw_klucz(klucze[glebokosc].pop(0), liczba_cyfr)  # natrafiono na lisc
    return tablica

#przerobienie drzewa o korzeniu x na listę kluczy
def rozdziel_klucze(x):
    drzewo = [[x]]
    warunek = True
    while warunek:  # przerabianie drzewa na klucze wg poziomow (glebokosci)

        warunek = False
        poziom = []
        for w in drzewo[-1]:
            if w is None:
                poziom.append(None)
                poziom.append(None)
            else:
                if w.left is not None:
                    poziom.append(w.left)
                    warunek = True
                else:
                    poziom.append(None)

                if w.right is not None:
                    poziom.append(w.right)
                    warunek = True
                else:
                    poziom.append(None)
        drzewo.append(poziom)
    return drzewo

#wyświetlenie drzewa o korzeniu x
def wyswietl_drzewo(x, liczba_cyfr=4, znak_liscia = 'N'):
    klucze = rozdziel_klucze(x)
    klucze = [[popraw_klucz(wartosc(i, znak_liscia[:liczba_cyfr]), liczba_cyfr) for i in j] for j in klucze]

    glebokosc = len(klucze)
    szerokosc = 2 ** glebokosc - 1
    tablica = [['    ' for i in range(szerokosc)] for j in range(glebokosc)]
    indeks_zero = szerokosc//2

    drzewo = wpisz_klucze(tablica, klucze, indeks_zero)

    drzewo_do_wyswietlenia = '-' * liczba_cyfr * szerokosc
    for poziom in drzewo[:-1]:
        tekst = ''
        for klucz in poziom:
            tekst += klucz
        tekst += '\n' + '-' * liczba_cyfr * szerokosc
        drzewo_do_wyswietlenia += '\n' + tekst
    return drzewo_do_wyswietlenia

# MAIN()

wybor_menu = int(input(
'''1. Losowe drzewo
2. Własne drzewo
3. Ustawienia
4. Wyjście

Wybór: '''))
wyczysc_ekran()

ustawienia_plik = open('ustawienia.txt', 'r') # wczytywanie ustawień
ustawienia = ustawienia_plik.readlines()
ustawienia_plik.close()

ustawienia = [i.strip() for i in ustawienia]

N = [int(i) for i in ustawienia[0].split()]
maks = int(ustawienia[1])
znak_liscia = ustawienia[2]
liczba_cyfr = len(str(maks))


if wybor_menu == 1: # losowe
    open('wyniki.txt', 'w').close()     # wyczyszczenie pliku 'wyniki.txt'
    for n in N:
        licznik_szukanie = 0
        licznik_dodawanie = 0
        licznik_usuwanie = 0

        for i in range(20):
            lista = losowa_lista(n, maks)
            tasuj(lista)

            korzen = nowy_korzen(lista[0])  # korzen drzewa
            for k in lista[1:]:             # wypelnianie drzewa elementami z listy
                dodaj(korzen, k)

            losowy_do_szukania = lista[np.random.randint(1, n)]     # element do wyszukania
            losowy_do_dodania = np.random.randint(0, maks)   # element do dodania
            losowy_do_usuniecia = lista[np.random.randint(1, n)]     # element do wyszukania


            licznik_szukanie  += szukaj(korzen, losowy_do_szukania, True)
            licznik_dodawanie += dodaj(korzen, losowy_do_dodania, True )
            licznik_usuwanie  += usun(korzen, losowy_do_usuniecia, True)

        licznik_szukanie /= 20
        licznik_dodawanie /= 20
        licznik_usuwanie /= 20

        tekst_do_zapisu = f'n = {n}\n\nLiczba operacji podczas:\nszukania: {licznik_szukanie}\ndodawania: {licznik_dodawanie}\nusuwania: {licznik_usuwanie}\n\n'
        wyniki_plik = open('wyniki.txt', 'a')
        wyniki_plik.write(tekst_do_zapisu)
        wyniki_plik.close()




elif wybor_menu == 2: # własne drzewo
    korzen = None
    wiadomosc = ''
    komenda = 'wyswietl'
    while komenda in ['dodaj', 'usun', 'wyswietl', 'help']:
        wyczysc_ekran()
        print(wiadomosc)
        wiadomosc = ''
        polecenie = input('Proszę podać polecenie (help - lista komend): ')
        polecenie = polecenie.split()
        komenda = polecenie[0].lower() # male litery
        if len(polecenie) > 1:
            klucz = int(polecenie[1])

        if komenda == 'help':
            wiadomosc = '''
- dodaj x, aby dodać do drzewa element z kluczem x,

- usun x, aby usunąć z drzewa element z kluczem x,

- wyswietl, aby wyświetlić drzewo,

- zakoncz, aby zakończyć działanie programu.
            '''

        if komenda == 'dodaj':     # dodaj element do wlasnego drzewa
            if klucz > maks:
                wiadomosc = 'Nie dodano elementu - niepoprawna wartość klucza!\n'
            else:
                if korzen is None:
                    korzen = nowy_korzen(klucz) # polecenie[1] - klucz
                else:
                    dodaj(korzen, klucz)
                wiadomosc = f'Dodano element o kluczu {klucz}\n'

        elif komenda == 'usun':      # usun element z wlasnego drzewa
            if szukaj(korzen, klucz) is None:
                wiadomosc = 'W drzewie nie ma takiego elementu!'
            else:
                if korzen.is_leaf() and korzen.key == klucz:
                    korzen = None
                else:
                    usun(korzen, klucz)
                wiadomosc = f'Usunięto element o kluczu {klucz}\n'

        elif komenda == 'wyswietl':  # usun element z wlasnego drzewa
            if korzen is None:
                wiadomosc = 'Drzewo jest puste'
            else:
                wiadomosc = wyswietl_drzewo(korzen, liczba_cyfr, znak_liscia) + '\n'

    pauza = input()

elif wybor_menu == 3: # ustawienia
    nowe_N, nowe_maks, nowe_znak_liscia = N, maks, znak_liscia
    nowe_N = ''
    for n in N:
        nowe_N += str(n)+' '
    while True:
        print(f'''Ustawienia:

1. Długości list:         {N}

2. Maks. wartość klucza:  {maks}

3. Oznaczenie liscia:     {znak_liscia}

4. Zapisz

5. Zakończ
''')
        wybor_ustawienia = int(input('Wybór: '))

        if wybor_ustawienia == 1:
            nowe_N = input('Proszę podać liczby oddzielone spacjami: ')

        elif wybor_ustawienia == 2:
            nowe_maks = input('Proszę podać maksymalną wartość kluczy: ')

        elif wybor_ustawienia == 3:
            nowe_znak_liscia = input('Proszę podać oznaczenie liścia: ')

        elif wybor_ustawienia == 4: # zapisywanie ustawień
            nowe_ustawienia = [str(nowe_N) + '\n', str(nowe_maks) + '\n', str(nowe_znak_liscia) + '\n']
            ustawienia_plik = open('ustawienia.txt', 'w')
            ustawienia_plik.writelines(nowe_ustawienia)
            ustawienia_plik.close()

        elif wybor_ustawienia == 5:
            break
        wyczysc_ekran()